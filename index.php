<!DOCTYPE html>
<html lang="es">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Plantilla básica de Bootstrap</title>
 
    <!-- CSS de Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet" media="screen">
    <link href="css/bootstrap-theme.min.css" rel="stylesheet" media="screen">
 
 
    <!-- librerías opcionales que activan el soporte de HTML5 para IE8 -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    
    <style>
      body{
        background-color: rgba(100,30,100,0.3);
      }

      h1{
        position: relative; top:20%;
      }

      table{
        background-color: white;
      }

      table th {
        text-align: center;
      }
      table tr{
        text-align: center;
      }

      header{
        background-image: url("imagenes/slide.jpg");
        text-align: center;
        color: white;

        height: 500px;
        margin-bottom: 50px;
        
      }
      footer{
        background-color: rgba(040,040,040,0.4);
        text-align: center;
        color: white;

        height: 120px;
        margin-top: 20px;

      }



   </style>

  </head>

  <body>
   
      <?php
        require ('classes/disco.class.php');
        require ('classes/coleccion.class.php');

        if (isset($_POST['enviar'])){
           
            $nombre=$_POST['nombre'];
            $anyo=$_POST['anyo'];
            $autor=$_POST['autor'];
            $numCancion=$_POST['numCancion'];

            move_uploaded_file($_FILES['portada']['tmp_name'], 'imagenes/'.$_FILES['portada']['name']);
           

           
            $linea="\r\n".$nombre. ';' .$anyo. ';' .$autor. ';' . $numCancion .';'. $_FILES['portada']['name'];

           
            $fichero=fopen('datos.txt', 'a');
            

            fwrite($fichero, $linea);

           
            fclose($fichero);

        }


        $coleccion= new Coleccion('Guía de discos <small>Ordenadicos</small>');


        $fichero=fopen('datos.txt','r');

          $linea=fgets($fichero);
          while($linea=fgets($fichero)){
            $partes=explode(';', $linea );
            $nombre=trim($partes[0]);
            $anyo=trim($partes[1]);
            $autor=trim($partes[2]);
            $numCancion=trim($partes[3]);
            $portada=trim($partes[4]);
           

            $coleccion->agregar(new Disco($nombre, $anyo, $autor,$numCancion,$portada));
          }

   
        fclose($fichero);



      ?>

      <header class="container-fluid"><?php include('includes/cabecera.php') ?></header>
    

      <div class="container">

<div class="row">
        <div class="col-md-8">
        <table class="table table-bordered">

          <tr>
            <th colspan="3"> <?php echo $coleccion->titulo; ?> </th>
          </tr>
          
          <tr>
            <th>Nombre</th>
            <th>Año</th>
            <th>Autor</th>
            <th>Canciones</th>
            <th>Portada</th>
          </tr>
          
            
          <?php foreach ($coleccion->listar() as $disco ) { ?>

          <tr>
            <td> <?php echo $disco->nombre ?> </td>
            <td><?php echo $disco->anyo ?> </td>
            <td><?php echo $disco->autor ?> </td>
            <td> <?php echo $disco->numCancion ?> </td>
            <td>
              <img src="imagenes/<?php echo $disco->portada; ?>" width="100">
              
                
            </td>
          </tr>


        
          <?php } ?> 

        

        </table>
      
       </div>


         <div class="col-md-4">
          <form action="index.php" method="post" enctype="multipart/form-data">

              <input type="text" name="nombre" placeholder="Escribe el nombre del disco" class="form-control"><br>
              <input type="text" name="anyo" placeholder="Escribe el año del disco" class="form-control"><br>
              <input type="text" name="autor" placeholder="Escribe el autor del disco" class="form-control"><br>
              <input type="text" name="canciones" placeholder="Canciones de disco" class="form-control"><br>
              <input type="file" name="portada" class="form-control"><br>
            

              <input type="submit" name="enviar" value="Enviar">


          </form>
    </div>

  </div>

 </div>
        
    


    <footer><?php include('includes/pie.php') ?></footer> 


 
    <!-- Librería jQuery requerida por los plugins de JavaScript -->
    <script src="js/jquery-3.3.1.min.js"></script>
 
    <!-- Todos los plugins JavaScript de Bootstrap (también puedes
         incluir archivos JavaScript individuales de los únicos
         plugins que utilices) -->
    <script src="js/bootstrap.min.js"></script>
  </body>
</html>